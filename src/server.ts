import { Server } from "@overnightjs/core";
import bodyParser from "body-parser";
import cors from "cors";
import connectToDB, {
  ProductRepository,
} from "./@core/repository/product.repository";
import { ProductController } from "./module/product/product.controller";

export class ProductServer extends Server {
  private productController: ProductController;
  private allowOrigin: string[] = ["http://localhost:4200"];
  private options: cors.CorsOptions;

  constructor() {
    super(true);
    this.productController = new ProductController();
    this.options = { origin: this.allowOrigin };
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(cors(this.options));
    this.app.use("/product", this.productController.router);
  }

  public start(port: number): void {
    console.log("Server Started");
    this.app.listen(port);
  }
}

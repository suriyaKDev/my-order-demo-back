import mongoose, { Model } from "mongoose";
import { IProduct, productSchema } from "../../@core/models/product.model";
import ProductRepository from "../../@core/repository/product.repository";

export class ProductService {
  private productRepository: ProductRepository;

  constructor() {
    this.productRepository = new ProductRepository();
  }

  public async getAll(limit: number, page: number): Promise<IProduct[]> {
    let productList: IProduct[];
    let skip: number;
    try {
      const countProduct = await this.productRepository.count()
      skip = ((page * limit) - limit);
      if (skip >= countProduct) {
        skip = countProduct;
      }
      console.log(`[GET ALL] ${page} | ${limit}  | ${skip} | ${countProduct}`);

      productList = await this.productRepository.getAll(limit, skip);
    } catch (err) {
      console.error(err);
      throw new Error("can't get all product");
    }
    return productList;
  }

  public async countProduct(): Promise<number> {
    let countProduct: number;
    try {
      countProduct = await this.productRepository.count()
    } catch (err) {
      console.error(err);
      throw new Error("can't count product");
    }
    return countProduct;
  }

  public async getById(id: string): Promise<IProduct | null> {
    let product: IProduct | null;
    try {
      product = await this.productRepository.getById(id);
    } catch (err) {
      console.error(err);
      throw new Error("product not found");
    }
    return product;
  }

  public async create(newProduct: IProduct): Promise<IProduct> {
    let _newProduct: IProduct;
    try {
      _newProduct = await this.productRepository.create(newProduct);
    } catch (err: any) {
      console.error(err);
      throw new Error("can't create new product");
    }
    return _newProduct;
  }

  public async update(id: string, product: IProduct | null): Promise<IProduct | null> {
    let updateProduct: IProduct | null;
    try {
      updateProduct = await this.productRepository.update(
        id,
        {
          productName: product?.productName,
          productPrice: product?.productPrice,
          productStock: product?.productStock,
        } as IProduct
      );
    } catch (err) {
      console.error(err);
      throw new Error("can't update product");
    }
    return updateProduct;
  }

  public async delete(id: string): Promise<IProduct | null> {
    let deleteProduct: IProduct | null;
    try {
      deleteProduct = await this.productRepository.delete(id);
    } catch (err) {
      console.error(err);
      throw new Error("can't delete new product");
    }
    return deleteProduct;
  }
}

import { IProduct } from "../../../@core/models/product.model";
import ProductRepository from "../../../@core/repository/product.repository";
import { ProductService } from "../product.service";

jest.mock('../../../@core/repository/product.repository')


describe('ProductService', () => {

    describe('Method create', () => {
        let createMock: jest.Mock;
        const mockProduct = { productName: 'test_product_form', productPrice: 20, productStock: 99 } as IProduct;
        beforeEach(() => {
            createMock = jest.fn();
            ProductRepository.prototype.create = createMock;
        })
        test('should create new product success', async () => {
            const expected = { productName: 'test_product_form', productPrice: 20, productStock: 99 } as IProduct;
            createMock.mockResolvedValueOnce(expected)
            const productService = new ProductService();
            const actual: IProduct = await productService.create(mockProduct);
            expect(expected).toEqual(actual);
        });
        test('should create new product failed', async () => {
            const expected: any = { productPrice: 0, productStock: 0 };
            try {
                createMock.mockRejectedValueOnce(expected);
                const productService = new ProductService()
                await productService.create(expected);
            } catch (err: any) {
                expect(err).toEqual(new Error("can't create new product"))
            }
        });
    })

    describe('Method getAll', () => {
        let getAllMock: jest.Mock;
        beforeEach(() => {
            getAllMock = jest.fn();
            ProductRepository.prototype.getAll = getAllMock;
        })
        test('should get first page of product success', async () => {
            const expected: IProduct[] = [
                { productName: 'test_product_form_1', productPrice: 20, productStock: 100 },
                { productName: 'test_product_form_2', productPrice: 30, productStock: 200 },
                { productName: 'test_product_form_3', productPrice: 40, productStock: 300 }
            ] as IProduct[];
            const limit: number = 10;
            const page: number = 1;
            getAllMock.mockResolvedValueOnce(expected);
            const productService = new ProductService();
            let actual: IProduct[] = await productService.getAll(limit, page);
            expect(expected).toEqual(actual);
        });
        test('should get all product failed', async () => {
            const expected: IProduct[] = [];
            const limit: number = 10;
            const page: number = 1;
            try {
                getAllMock.mockRejectedValueOnce(expected);
                const productService = new ProductService();
                await productService.getAll(limit, page);
            } catch (err) {
                expect(err).toEqual(new Error("can't get all product"))
            }
        })

    })

    describe('Method getById', () => {
        let getById: jest.Mock;
        const mockId: string = "628701d42ab416ed0b8e3132";
        beforeEach(() => {
            getById = jest.fn();
            ProductRepository.prototype.getById = getById;
        })
        test('should get product by id success', async () => {
            const expected: string = "628701d42ab416ed0b8e3132";;
            getById.mockResolvedValueOnce(expected);
            const productService = new ProductService();
            let actual: IProduct | null = await productService.getById(mockId);
            expect(expected).toEqual(actual);
        });
        test('should get product by id failed', async () => {
            const expected: string = "628701d42ab";
            try {
                getById.mockRejectedValueOnce(expected);
                const productService = new ProductService();
                await productService.getById(mockId);
            } catch (err) {
                expect(err).toEqual(new Error("product not found"));
            }
        })
    })

    describe('Method update', () => {
        let update: jest.Mock;
        const mockId: string = "628701d42ab416ed0b8e3132";
        const mockProduct: IProduct = { productName: 'test_product_form_before', productPrice: 20, productStock: 99 } as IProduct;
        beforeEach(() => {
            update = jest.fn();
            ProductRepository.prototype.getById = update;
        })
        test('should update product by id success', async () => {
            const expectedId: string = "628701d42ab416ed0b8e3132";
            const expectedProduct: IProduct = { productName: 'test_product_form_after', productPrice: 20, productStock: 99 } as IProduct;
            update.mockResolvedValueOnce(expectedId);
            update.mockResolvedValueOnce(expectedProduct);
            const productService = new ProductService();
            let actual: IProduct | null = await productService.update(mockId, mockProduct);
            expect(expectedProduct).not.toEqual(actual);
        });
        test('should update product by id failed', async () => {
            const expectedId: string = "628701d42ab416ed0b8e3132";
            const expectedProduct: any = { productName: "test_product_after", productStock: 99 } as IProduct;
            try {
                update.mockResolvedValueOnce(expectedId);
                update.mockResolvedValueOnce(expectedProduct);
                const productService = new ProductService();
                await productService.update(expectedId, expectedProduct);
            } catch (err) {
                expect(err).toEqual(new Error("can't update product"));

            }
        });
    })

    describe('Method delete', () => {
        let deleted: jest.Mock;
        const mockId: string = "628701d42ab416ed0b8e3132";
        beforeEach(() => {
            deleted = jest.fn();
            ProductRepository.prototype.delete = deleted;
        })
        test('should delete product by id success', async () => {
            const expectedId: string = "628701d42ab416ed0b8e3132";
            const expectedProduct = { productName: 'test_product_form', productPrice: 20, productStock: 99, deleted: true } as IProduct;
            deleted.mockResolvedValueOnce(expectedId);
            const productService = new ProductService();
            let actual = await productService.delete(mockId) as IProduct;
            expect(expectedId).toEqual(mockId);
        });
        test('should delete product by id failed', async () => {
            const expectedId: string = "628701d42ab";
            try {
                deleted.mockRejectedValueOnce(expectedId);
                const productService = new ProductService();
                await productService.delete(expectedId);
            } catch (err) {
                expect(err).toEqual(new Error("can't delete new product"));
            }
        });
    })

})
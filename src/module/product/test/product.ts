import supertest from "supertest";
const request = supertest.agent("http://localhost:3000/product");

describe("Product API", () => {
  test("GET list products", () => {
     request.get("/").expect(200);
  });

  test("GET product by id", async () => {
    return request
      .get("/show/62822eafadda3a084175949a")
      .expect(200)
      .then((response) => {
        expect(response.body.productName).toEqual("HelloLGD");
      });
  });

  test("GET empty id should return empty", async () => {
    return request.get("/show/").then((response) => {
      expect(response.body).toEqual({});
    });
  });

  test("GET incorrect id should return error CastError", async () => {
    return request.get("/show/62822eafadda3a0").then((response) => {
      expect(response.body.name).toEqual("CastError");
    });
  });

  test("GET product by name", async () => {
    return request
      .get("/find/HelloLGD")
      .expect(200)
      .then((response) => {
        expect(response.body.productName).toEqual("HelloLGD");
      });
  });

  test("GET product by name empty", async () => {
    return request.get("/find/Hello").then((response) => {
      expect(response.body).toEqual(null);
    });
  });

  test("GET product by name not found", async () => {
    return request.get("/find/").expect(404);
  });

  test("POST insert new product", async () => {
    return request
      .post("/add")
      .send({
        productName: "jestProduct",
        productPrice: 100,
        productStock: 50,
      })
      .expect(200);
  });

  test("POST update product by id", async () => {
    return request
      .post("/edit/628241dcadda3a084175949e")
      .send({
        productName: "updateJestProduct",
        productPrice: 250,
        productStock: 999,
      })
      .expect(200);
  });

  test("POST delete product by id", async () => {
    return request
      .post("/delete/62824207adda3a08417594a3")
      .send({
        status: "inactive",
      })
      .expect(200);
  });
});

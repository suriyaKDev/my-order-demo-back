import express, { NextFunction, Request, Response, Router } from "express";
import { IProduct } from "../../@core/models/product.model";
import { ProductService } from "./product.service";

export class ProductController {
  public service: ProductService;

  public router: Router = express.Router();

  constructor() {
    this.service = new ProductService();
    this.initializeRoutes();
  }

  private initializeRoutes(): void {
    this.router.get("/", this.getAll.bind(this));
    this.router.get("/show/:id", this.getProductById.bind(this));
    // this.router.get("/find/:name", this.getProductByName.bind(this));
    this.router.post("/add", this.create.bind(this));
    this.router.put("/edit/:id", this.updateProductById.bind(this));
    this.router.delete("/delete/:id", this.deleteProductById.bind(this));
  }

  public async getAll(req: Request, res: Response): Promise<void> {
    try {
      const page: any = req.query.page || 1;
      const limit: any = req.query.limit || 10;
      const countProduct = await this.service.countProduct();
      const productList: IProduct[] = await this.service.getAll(limit, page);
      console.log("[SERVER] : GET ALL PRODUCT", countProduct);
      res.status(200).json(
        { data: productList, count: countProduct }
      );
    } catch (err) {
      res.status(404).send({
        data: null,
        error: err
      });
    }
  }

  public async create(req: Request, res: Response): Promise<void> {
    try {
      const newProduct: IProduct = await this.service.create(req.body);
      console.log("[SERVER] : CREATE NEW PRODUCT");
      res.status(200).json(newProduct);
    } catch (err) {
      res.status(404).json(err);
    }
  }

  public async getProductById(req: Request, res: Response): Promise<void> {
    try {
      const id = req.params.id;
      const product: IProduct | null = await this.service.getById(id);
      console.log("[SERVER] : GET PRODUCT BY ID");
      res.status(200).json(product);
    } catch (err) {
      res.status(404).send(err);
    }
  }


  public async updateProductById(req: Request, res: Response): Promise<void> {
    try {
      const id = req.params.id;
      const setProduct = req.body;
      const product: IProduct | null = await this.service.update(id, setProduct);
      console.log("[SERVER] : UPDATE PRODUCT");
      res.status(200).json(product);
    } catch (err) {
      res.status(404).send(err);
    }
  }

  public async deleteProductById(req: Request, res: Response): Promise<void> {
    try {
      const id = req.params.id;
      const product: IProduct | null = await this.service.delete(id);
      console.log("[SERVER] : Delete Product Successfully\n", product);
      res.status(200).json(product);
    } catch (err) {
      res.status(404).send(err);
    }
  }
}

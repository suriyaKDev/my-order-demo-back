import mongoose, { Model } from "mongoose";
import { IProduct, productSchema } from "../models/product.model";

export class ProductRepository {
  private url: string = "mongodb://localhost:27017/productDB";
  private filter: boolean = false;
  private productRepository: Model<IProduct>;
  constructor() {
    this.connectToDB()
    this.productRepository = mongoose.model<IProduct>("Product", productSchema);
  }
  public connectToDB(): void {
    mongoose
      .connect(this.url)
      .then(() => {
        console.log("Connected to Database");
      })
      .catch((err) => {
        mongoose.connection.on("disconnect", this.connectToDB);
        console.log("Error connecting to Database: ", err);
      });
  };



  public async getAll(_limit: number, _page: number): Promise<IProduct[]> {
    console.log(`[SHOW]: ${_page} OF [LIMIT] : ${_limit}`);
    return this.productRepository.find({ deleted: this.filter }).skip(_page).limit(_limit).sort({ createdAt: -1 });
  }

  public async count(): Promise<number> {
    return this.productRepository.count({ deleted: this.filter });
  }

  public async getById(id: string): Promise<IProduct | null> {
    return this.productRepository.findOne({ _id: id });
  }

  public async getByName(name: string): Promise<IProduct | null> {
    return this.productRepository.findOne({ productName: name });
  }

  public async create(newProduct: IProduct): Promise<IProduct> {
    return this.productRepository.create(newProduct);
  }

  public async update(id: string, product: IProduct | null): Promise<IProduct | null> {
    return this.productRepository.findOneAndUpdate(
      { _id: id },
      {
        productName: product?.productName,
        productPrice: product?.productPrice,
        productStock: product?.productStock,
      },
      { new: true }
    );
  }

  public async delete(id: string): Promise<IProduct | null> {
    return this.productRepository.findOneAndUpdate({ _id: id }, { deleted: true });
  }
}

export default ProductRepository;

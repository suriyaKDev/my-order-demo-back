import { Schema } from "mongoose";

export interface IProduct extends Document {
  productName: string;
  productPrice: number;
  productStock: number;
  deleted: boolean;
  timestamps?: Date
}


export const productSchema = new Schema<IProduct>(
  {
    productName: { type: String, required: true },
    productPrice: { type: Number, required: true },
    productStock: { type: Number, required: true },
    deleted: { type: Boolean, default: false }
  },
  { timestamps: true }
);
